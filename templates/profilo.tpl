{* pagina profilo  *}

{config_load file="config.conf"}
<html>
<head >
<title> Diplomati ITIS </title>


{include file="header.tpl"}

</head>

<body>
<div class="container">


<!--<header id="header2" class="slide">     
		<ul id="navToggle" class="burger slide">  
    <li></li><li></li><li></li>
		</ul>-->

{include file="intestazione.tpl"}
{if $smarty.session.auth == $smarty.const.AZIENDA}
  <h1>AZIENDE ITIS - Profilo</h1>
{elseif $smarty.session.auth == $smarty.const.DIPLOMATO}
  <h1>DIPLOMATI ITIS - Profilo</h1>
{/if} 
<!--<h1>{#titolo#} - Profilo</h1>-->

<script language="JavaScript">

  function validate(f)
  {
    
    if(f.password.value != f.passwordconfirm.value)
    {
      alert("La password non coicide");
      return false;
    }

    return true;
  }

</script>

</header>

{include file="menu.tpl"}

	<div class="content slide">     <!--	Add "slideRight" class to items that move right when viewing Nav Drawer  -->
  
		<ul class="responsive">

			<li class="header-section">
			</li>

<li class="body-section">
{if $error}<p id=error> ERROR: {$error} {/if}

<form name=profile action="{$SCRIPT_NAME}?action=update" method=post onsubmit="return validate(this);" enctype="multipart/form-data">
<button id="button1" type="submit" class="btn btn-primary btn-lg">Salva</button>
<button  onclick="window.location='index.php'" id="button2" type="reset" class="btn btn-danger btn-lg">Annulla</button>
<input type=hidden name=id value={$valori.ID}>



<table class="table-fill">

{if $smarty.session.auth == $smarty.const.AZIENDA}

  <tr> 
    <td>Denominazione</td>
    <td><input name=denominazione type=text value="{$valori.denominazione}" required="required" ></td>
  </tr>
  <tr> 
    <td>Residenza</td>
    <td><input name=residenza type=text value="{$valori.residenza}" required="required"></td>
  </tr>
  <tr> 
    <td>Sito</td>
    <td><input name=sito type=text value="{$valori.sito}"></td>
  </tr>

{elseif $smarty.session.auth == $smarty.const.DIPLOMATO}
  <tr> 
    <td>Cognome</td>
    <td><input name=cognome type=text value="{$valori.cognome}" >
    </td>
  </tr>
  <tr> 
    <td>Nome</td>
    <td><input name=nome type=text value="{$valori.nome}" ></td>
  </tr>
  <tr> 
    <td>Data di nascita</td>
    <td><input name=datanascita type=date  value='{$valori.datanascita|date_format:"%Y-%m-%d"}' ></td>
  </tr>
  <tr> 
    <td>Anno di diploma</td>
    <td><input name=annodiploma type=number  value="{$valori.annodiploma}" ></td>
  </tr>
  <tr> 
    <td>Cellulare</td>
    <td><input name=cellulare type=number  value="{$valori.cellulare}" ></td>
  </tr>
  <tr> 
    <td>Laurea</td>
    <td><select  name=laurea value="{$valori.laurea}">
    <option value="" selected disabled hidden>Seleziona</option>
  <option value="1">Si</option>
  <option value="2">No</option>
</select></td>
  </tr>
  <tr> 
    <td>Curriculum</td>
    <td>
    {if $valori.curriculumlink }
      <a href=profilo.php?download=curriculum><img src="img/pdf.png">File attuale</a>
    {else}
    <img src="img/nopdf.png">
    Nessun curriculum allegato
    {/if} 
    <p STYLE="text-align:right"><input name=curriculumfile type=file ></td></p>
  </tr>

  <tr> 
    <td>Occupazione</td>
    <td>
      <select name=ID_Toccupazioni>
      {html_options options=$occupazioni selected=$valori.ID_Toccupazioni}
      </select>
      </td>
  </tr>


{/if}
  <tr> 
    <td>Descrizione</td>
    <td>
      <select name=ID_Tindirizzistudio>
      {html_options options=$indirizzistudio selected=$valori.ID_Tindirizzistudio}
      </select>
      </td>
  </tr>

  <tr> 
    <td>E-mail</td>
    <td><input name=email type=text value="{$valori.email}" disabled ></td>
  </tr>

  <tr> 
    <td>Password</td>
    <td><input name=password type=password value="{$valori.password}" 
      {literal}pattern=".{6,}" {/literal} title="Almeno 6 caratteri">
      </td>
  </tr>
  
  <tr> 
    <td>Conferma password</td>
    <td><input name=passwordconfirm type=password value="{$valori.password}" 
      {literal}pattern=".{6,}" {/literal} title="Almeno 6 caratteri">
   </td>
  </tr>
  

</table>

</form>


</li>
			<li class="footer-section">
				{include file="footer.tpl"}
			</li>
		</ul>
	</div>
</div>

</body>
</html>

