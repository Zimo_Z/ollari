{* pagina principale  *}

{config_load file="config.conf"}
<html>
<head >
<title> Diplomati ITIS </title>


{include file="header.tpl"}

</head>

<body>
<div class="container">



<!--<header class="slide">    
		<ul id="navToggle" class="burger slide">  
    <li></li><li></li><li></li>
		</ul>-->


{include file="intestazione.tpl"}
<h1>{if $aziende }AZIENDE ITIS{elseif (($diplomati) && ($smarty.session.auth == $smarty.const.AZIENDA))} AZIENDE ITIS - Diplomati{elseif $diplomati}DIPLOMATI ITIS{elseif $diplomati}{else}HOME{/if} </h1>

</header>

{include file="menu.tpl"}


	<div id="main" class="content slide">     <!--	Add "slideRight" class to items that move right when viewing Nav Drawer  -->
		<ul class="responsive">

			<li class="header-section">
			</li>

<li class="body-section">
<form name=lista action=index.php method=post>

<input type=hidden name=lista value={$smarty.request.lista}>

{if $aziende || $diplomati}
<button id="print" onclick="printContent('mytable');" class="btn btn-primary mb-2" style="float: right;" >Esporta/Stampa</button>
<br>
<br>
<br>
<script>
function printContent(el){
var restorepage = $('body').html();
var printcontent = $('#' + el).clone();
$('body').empty().html(printcontent);
window.print();
$('body').html(restorepage);
}
</script>

<table style="margin: auto; width: 90%;" >

  <tr>
  
  {if ($smarty.session.auth == $smarty.const.AZIENDA) && $diplomati}

  <script>
/*$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});*/
</script>
<script src="/js/table-search.js"></script>
<td>Ricerca:</td>
<td>
  <select id="field-name">
    <option value="nome">Nome</option>
    <option value="cognome">Cognome</option>
    <option value="indirizzo">Indirizzo di Studio</option>
    <option value="anno">Anno diploma</option>
    <option value="descrizione">Descrizione</option>
    <option value="laurea">Laurea</option>
  </select>
</td>
<td>

<!--
  Bolo - input
  Modificato id: myInput -> search-field
-->
<td>
  <input id="search-field" type="text">
</td>
<td>
{*Chiamata tooltip tooltip AS *}
  <a  style="color:black;" data-toggle="tooltip" title="La ricerca viene effettuata a partire dal primo carattere del campo" >Iniziale:</a> <input id="is-strict" type="checkbox"><br>
  
<button id="print" onclick="reload()" class="btn btn-primary " style="float: left;" >Tutti</button>
</td>

{*
  <td>Indirizzo Studio</td>
  <td>  
    <select name=indirizzo OnChange="this.form.submit();">
    <option value="">TUTTI</option>
    {html_options options=$indirizzistudio  selected=$smarty.request.indirizzo}
    </select>
  </td>


  <td>Occupazione</td>
  <td>  
    <select name=occupazione OnChange="this.form.submit();">
    <option value="" >TUTTI</option>
    {html_options options=$occupazioni selected=$smarty.request.occupazione}
    </select>
  </td>

  <td>Laurea</td>
  <td>  
    <select name=laurea OnChange="this.form.submit();">
    <option value="">TUTTI</option>
    {html_options options=$laurea selected=$smarty.request.laurea}
    </select>
  </td>*}
  {/if}
  </tr>
  
</table>

{else}

<p id="fontupper">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac tempor ante. Vestibulum eu lorem sed leo vulputate efficitur. Fusce mattis convallis magna, eu feugiat ex lobortis eu. Phasellus mattis mattis ornare. Nulla sed pellentesque massa, ut venenatis ante. Praesent tincidunt vel ante quis aliquam. Phasellus ac venenatis eros. Donec non est ut ante bibendum auctor ut eget ex. In sodales metus vel lectus ultrices commodo. Nulla feugiat tempus leo sed porttitor. Curabitur euismod tincidunt varius. Nullam pharetra a justo ac consequat. Quisque ut posuere libero. Proin malesuada scelerisque leo dignissim varius. Suspendisse fringilla ante id ligula tempor ullamcorper.

Quisque eu lectus in leo consectetur scelerisque sed non nibh. Nunc condimentum convallis mi ut elementum. Aliquam a quam nec lorem sollicitudin ultrices. In ut risus mattis, scelerisque dui quis, lobortis lectus. Quisque commodo, sem quis convallis luctus, nulla eros malesuada arcu, nec lobortis ipsum metus ac nibh. Aliquam blandit tempus velit, condimentum tincidunt leo mollis et. In at mattis ante, ac convallis nibh. Sed eget risus nisl. Fusce commodo volutpat ex ut interdum. Phasellus leo tellus, pulvinar ut nulla eu, semper hendrerit dui. Vivamus blandit, elit nec rhoncus commodo, dolor odio pellentesque lectus, id dictum lectus justo vel purus. Fusce condimentum turpis eget imperdiet luctus. Ut vel elit dui. Nunc et ipsum vulputate, vehicula lacus sed, luctus sem. Mauris aliquet cursus ligula, sed fermentum turpis pellentesque sit amet.

Aliquam fringilla ante eu metus tincidunt pulvinar. Fusce nisi eros, feugiat molestie turpis vel, auctor dapibus velit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam ultricies nisl et ante lacinia, a sagittis est gravida. Nam rhoncus quam leo. Morbi eros leo, molestie nec lacus vitae, euismod dapibus ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean aliquam faucibus turpis, eu imperdiet ligula auctor vitae. Aliquam vel velit tortor. Praesent pharetra varius est, vitae porttitor neque euismod vitae. Quisque at sem id erat semper vehicula.

Phasellus vehicula convallis dignissim. Proin in mauris tincidunt, pretium dolor vel, convallis lacus. Praesent rhoncus eget purus sed blandit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla a ante at ipsum scelerisque semper non eu enim. Proin quam justo, vulputate in malesuada a, viverra vel urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec eget gravida est. Phasellus finibus faucibus efficitur. Sed vitae est congue, pharetra dolor sit amet, sagittis lectus. Donec eleifend interdum ante, sed scelerisque nisl pulvinar eu. Nulla in interdum nulla, vel ullamcorper tellus. Donec vitae vestibulum diam, vitae ornare ex. Pellentesque augue nibh, congue in sollicitudin eget, pharetra quis magna.

Aenean pretium diam vel neque vehicula, a pulvinar diam accumsan. Donec dignissim maximus gravida. Morbi maximus massa risus, a pulvinar tortor sagittis et. Fusce ornare egestas ipsum at efficitur. Ut ex urna, iaculis ut sodales quis, faucibus eget nisi. Curabitur mattis, neque ut semper laoreet, lectus lacus porttitor leo, in scelerisque magna velit quis ante. Suspendisse eleifend, sem sed pretium fermentum, mauris massa venenatis libero, hendrerit varius lacus massa at mi. Fusce eu pharetra mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget est eu metus posuere tristique. Suspendisse potenti. Mauris mattis viverra euismod. Suspendisse a libero odio. In elementum neque eu tempus viverra. Phasellus eros quam, congue eget dui ac, placerat scelerisque tellus.</p>


{/if}

<table id="mytable" class="table-fill">
{if $aziende}

 <tr>
  <th>Denominazione</th><th>Residenza</th><th>Indirizzo</th>
 </tr>
  {foreach $aziende as $valori}
  <tr>
    <td>{$valori.denominazione}</td>
    <td> {$valori.residenza} </td><td> {$valori.descrizione}</td>
  </tr>
  {/foreach}

{/if}


{if $diplomati && ($smarty.session.auth == $smarty.const.DIPLOMATO) || $diplomati && ($smarty.session.auth == $smarty.const.AZIENDA)}
 <tr>
  <th>Nome</th><th>Cognome</th><th>Indirizzo di Studio</th> <th>Anno diploma</th><th>Descrizione</th><th>Laurea</th>
 </tr>
  {foreach $diplomati as $valori}
  <tr>
    <td data-option="nome">{$valori.nome}</td><td data-option="cognome">{$valori.cognome}</td><td data-option="indirizzo">{if $valori.ID_Tindirizzistudio == 1}Informatica e telecomunicazioni{elseif $valori.ID_Tindirizzistudio == 2}Logistica e trasporti{elseif $valori.ID_Tindirizzistudio == 3}Chimica materiali e biotecnologie{elseif $valori.ID_Tindirizzistudio == 4}Elettronica e elettrotecnica{else}Meccanica meccantronica e energia{/if}</td>
    <td data-option="anno">{$valori.annodiploma}</td><td data-option="descrizione">{$valori.descrizione}</td>
    <td data-option="laurea">{if $valori.laurea == 1}Si{elseif $valori.laurea == 0}No{else}Non specificato{/if}</td>
  </tr>
  {/foreach}
{/if}

{if ($smarty.session.auth != $smarty.const.AZIENDA) && $diplomati}
  <p id="fontdiplomati">Studenti registrati nel portale: 
  <span id="fontdiplomati" class="counter">{sizeof($diplomati)}</span>
   <script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="/js/jquery.counterup.min.js"></script></p>
  {/if}

</table>
</form>




<script src="/js/index.js"></script>

</li>
			<li class="footer-section">
				{include file="footer.tpl"}
			</li>
		</ul>
	</div>
</div>

</body>
</html>
