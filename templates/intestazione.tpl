
{if $smarty.session.auth == $smarty.const.AZIENDA}
<header id="coloraziende" class="slide">     <!--	Add "slideRight" class to items that move right when viewing Nav Drawer  -->
		<ul id="navToggle" class="burger slide">  
    <li></li><li></li><li></li>
	</ul>
{elseif $smarty.session.auth == $smarty.const.DIPLOMATI}
<header id="colordiplomati" class="slide">     <!--	Add "slideRight" class to items that move right when viewing Nav Drawer  -->
		<ul id="navToggle" class="burger slide">  
    <li></li><li></li><li></li>
	</ul>
{else}
<header class="slide">     <!--	Add "slideRight" class to items that move right when viewing Nav Drawer  -->
		<ul id="navToggle" class="burger slide">  
    <li></li><li></li><li></li>
</ul>
{/if}