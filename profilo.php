<?php
require('setup.php');

$smarty = new SmartyItis;

$smarty->ValidateSession();

$smarty->connectdb();


$id = $_REQUEST["id"];

if($_REQUEST["download"] == "curriculum")
{
  $name = null;
  $id = $_SESSION["userid"];
  
  //Invio il curriculum associato all'ID
  $file = $smarty->GetNameCurriclum($id,$name);
  header("Content-Type: application/force-download");
  header('Content-Disposition: attachment; filename="'. basename($file) . '"');
  header('Content-Length: ' . filesize($file));
  readfile($file);

  exit(0);
}

$smarty->GetListBox();

if($_REQUEST["action"] == "update")
{

    


  if($_SESSION["auth"] == AZIENDA)
  {
    $sqlupdate = "UPDATE Taziende SET ";
      
    foreach($_POST as $key => $data)
    {
      switch($key)
      {
        //campi da non inserire automaticamente
        case "id":
        case "email":
        case "passwordconfirm":

          break;
        default:
          $sqlupdate .= " $key = '$data' ,";
      }
    } 
    
    $sqlupdate = rtrim($sqlupdate,',');

    $sqlupdate .= " WHERE ID = $id";

    $esito = $smarty->pdo->exec($sqlupdate);
    if(!$esito)
    {
      $error = $smarty->pdo->errorInfo();
      if($error[1] > 0)
      {
        $smarty->assign("error","SQL ERROR:$error[2]");
      }
    }


  }

  if($_SESSION["auth"] == DIPLOMATO)
  {


    $sqlupdate = "UPDATE Tdiplomati SET ";
      
    foreach($_POST as $key => $data)
    {
      switch($key)
      {
        //campi da non inserire automaticamente
        case "id":
        case "email":
        case "curriculumfile":
        case "passwordconfirm":

          break;
        default:
          $sqlupdate .= " $key = '$data' ,";
      }
    } 

    if( is_uploaded_file($_FILES['curriculumfile']['tmp_name']))
    {
        
        if(($_FILES["curriculumfile"]["type"] == "application/pdf"))
        { 
          $name ="";
          
          $store = $smarty->GetNameCurriclum($id,$name); 
          move_uploaded_file($_FILES['curriculumfile']['tmp_name'],$store);
          $sqlupdate .= "curriculumlink = '$name'";
        }
        else
        {
          $sqlupdate = rtrim($sqlupdate,',');
          $smarty->assign("error","File non compatibile, inviare solo PDF");
        }

    }
    else
    {
      $sqlupdate = rtrim($sqlupdate,',');
    }

    $sqlupdate .= " WHERE ID = $id";

    $esito = $smarty->pdo->exec($sqlupdate);
    if(!$esito)
    {
      $error = $smarty->pdo->errorInfo();
      if($error[1] > 0)
      {
        $smarty->assign("error","SQL ERROR:$error[2]");
      }
    }

  }


}





if($_SESSION["auth"] == AZIENDA)
{
  $sql ="SELECT * FROM Taziende
   WHERE ID= $_SESSION[userid]
   "; 

}

if($_SESSION["auth"] == DIPLOMATO)
{
  $sql ="SELECT * FROM Tdiplomati
   WHERE ID= $_SESSION[userid]
   "; 

}

$ret = $smarty->pdo->query($sql);
if($ret)
{
  $d = $ret->fetch();
  $smarty->assign("valori",$d);
}




$smarty->display("profilo.tpl");
#echo  "<link  rel = \"stylesheet\"  href = \"css/diplomati.css\" > ";
?>
