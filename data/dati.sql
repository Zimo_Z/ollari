INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (1,"Eos srl","Parma","www.eos.it","eos@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (2,"Eos1 srl","Parma","www.eos.it","eos@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (3,"Eos2 srl","Parma","www.eos.it","eos1@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (4,"Eos3 srl","Parma","www.eos.it","eos2@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (5,"Eos4 srl","Parma","www.eos.it","eos3@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (6,"Eos5 srl","Parma","www.eos.it","eos4@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (7,"Eos6 srl","Parma","www.eos.it","eos6@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (8,"Eos7 srl","Parma","www.eos.it","eos7@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (9,"Eos8 srl","Parma","www.eos.it","eos5@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (10,"Eos9 srl","Parma","www.eos.it","eos8@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (11,"Eos10 srl","Parma","www.eos.it","eos9@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (12,"Eos11 srl","Parma","www.eos.it","eos11@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (13,"Eos12 srl","Parma","www.eos.it","eos10@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES (14,"Eos13 srl","Parma","www.eos.it","eos12@pr.it","12345678","01/01/20 00:00:00",1);
INSERT INTO `Tdiplomati` (`ID`, `cognome`, `nome`, `datanascita`, `password`, `annodiploma`, `residenza`, `email`, `cellulare`, `laurea`, `curriculumlink`, `ID_Tindirizzistudio`, `ID_Toccupazioni`) VALUES (1,"Ollari","Paolo","01/25/63 00:00:00","1234567",NULL,"Parma","paolo.ollari@itis.pr.it","3386838900",NULL,"ollaripaolo63.pdf",1,4);
INSERT INTO `Tindirizzistudio` (`ID`, `descrizione`) VALUES (1,"Informatica e telecomunicazioni");
INSERT INTO `Tindirizzistudio` (`ID`, `descrizione`) VALUES (2,"Logistica e trasporti");
INSERT INTO `Tindirizzistudio` (`ID`, `descrizione`) VALUES (3,"Chimica materiali e biotecnologie");
INSERT INTO `Tindirizzistudio` (`ID`, `descrizione`) VALUES (4,"Elettronica e elettrotecnica");
INSERT INTO `Tindirizzistudio` (`ID`, `descrizione`) VALUES (5,"Meccanica meccantronica e energia");
INSERT INTO `Toccupazioni` (`ID`, `descrizione`) VALUES (1,"non occupato");
INSERT INTO `Toccupazioni` (`ID`, `descrizione`) VALUES (2,"occupato");
INSERT INTO `Toccupazioni` (`ID`, `descrizione`) VALUES (3,"università");
INSERT INTO `Toccupazioni` (`ID`, `descrizione`) VALUES (4,"università disponibile");


