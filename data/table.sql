-- 
-- Database: itisdiplomati
-- 

CREATE TABLE IF NOT EXISTS Taziende (
  `ID` int(11) NOT NULL,
  denominazione varchar(255),
  residenza varchar(255),
  sito varchar(255),
  email varchar(100),
  password varchar(255),
  datascadenza datetime,
  `ID_Tindirizzistudio` int(11)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS Tdiplomati (
  ID int(11) NOT NULL,
  cognome varchar(255),
  nome varchar(255),
  datanascita datetime,
  password varchar(255),
  annodiploma int(11),
  residenza varchar(255),
  email varchar(100),
  cellulare varchar(255),
  laurea  int(11),
  curriculumlink varchar(255),
  `ID_Tindirizzistudio` int(11),
  `ID_Toccupazioni` int(11)

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS Tindirizzistudio (
  ID int(11) NOT NULL,
  descrizione varchar(255)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS Toccupazioni (
  ID int(11) NOT NULL,
  descrizione varchar(255)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;






  

