-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 18, 2019 alle 17:24
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itisprdiplomati`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `taziende`
--

CREATE TABLE `taziende` (
  `ID` int(11) NOT NULL,
  `denominazione` varchar(255) DEFAULT NULL,
  `residenza` varchar(255) DEFAULT NULL,
  `sito` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `datascadenza` datetime DEFAULT NULL,
  `ID_Tindirizzistudio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `taziende`
--

INSERT INTO `taziende` (`ID`, `denominazione`, `residenza`, `sito`, `email`, `password`, `datascadenza`, `ID_Tindirizzistudio`) VALUES
(1, 'Eos srl', 'Parma', 'www.eos.it', 'eos@pr.it', '12345678', '2001-01-20 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `tdiplomati`
--

CREATE TABLE `tdiplomati` (
  `ID` int(11) NOT NULL,
  `cognome` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `datanascita` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `annodiploma` int(11) DEFAULT NULL,
  `residenza` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cellulare` varchar(255) DEFAULT NULL,
  `laurea` int(11) DEFAULT NULL,
  `curriculumlink` varchar(255) DEFAULT NULL,
  `ID_Tindirizzistudio` int(11) DEFAULT NULL,
  `ID_Toccupazioni` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tdiplomati`
--

INSERT INTO `tdiplomati` (`ID`, `cognome`, `nome`, `datanascita`, `password`, `annodiploma`, `residenza`, `email`, `cellulare`, `laurea`, `curriculumlink`, `ID_Tindirizzistudio`, `ID_Toccupazioni`) VALUES
(1, 'Ollari', 'Paolo', '2019-02-07 00:00:00', '1234567', 2015, 'Parma', 'paolo.ollari@itis.pr.it', '3386838900', 1, 'ollaripaolo63.pdf', 1, 1),
(2, 'Ollari', 'Paolo1', '0000-00-00 00:00:00', '1234567', 2014, 'Parma', 'paolo1.ollari@itis.pr.it', '3386838900', 2, 'ollaripaolo63.pdf', 1, 4),
(3, 'Ollari', 'Paolo2', '0000-00-00 00:00:00', '1234567', 2015, 'Parma', 'paolo2.ollari@itis.pr.it', '3386838900', 2, 'ollaripaolo63.pdf', 1, 4),
(4, 'Ollari', 'Paolo3', '0000-00-00 00:00:00', '1234567', 2016, 'Parma', 'paolo3.ollari@itis.pr.it', '3386838900', 1, 'ollaripaolo63.pdf', 1, 4),
(5, 'Ollari', 'Paolo4', '0000-00-00 00:00:00', '1234567', 2015, 'Parma', 'paolo4.ollari@itis.pr.it', '3386838900', 1, 'ollaripaolo63.pdf', 1, 4),
(6, 'Ollari', 'Paolo5', '0000-00-00 00:00:00', '1234567', 2015, 'Parma', 'paolo5.ollari@itis.pr.it', '3386838900', 1, 'ollaripaolo63.pdf', 1, 4),
(7, 'Ollari', 'Paolo6', '0000-00-00 00:00:00', '1234567', 2018, 'Parma', 'paolo6.ollari@itis.pr.it', '3386838900', 2, 'ollaripaolo63.pdf', 1, 4),
(8, 'Ollari', 'Paolo7', '0000-00-00 00:00:00', '1234567', 2016, 'Parma', 'paolo7.ollari@itis.pr.it', '3386838900', 2, 'ollaripaolo63.pdf', 1, 4),
(9, 'Ollari', 'Paolo8', '0000-00-00 00:00:00', '1234567', 2016, 'Parma', 'paolo8.ollari@itis.pr.it', '3386838900', 2, 'ollaripaolo63.pdf', 1, 4),
(10, 'Ollari', 'Paolo9', '0000-00-00 00:00:00', '1234567', 2018, 'Parma', 'paolo9.ollari@itis.pr.it', '3386838900', 1, 'ollaripaolo63.pdf', 1, 4),
(11, 'Ollari', 'Paolo10', '0000-00-00 00:00:00', '1234567', 23, 'Parma', 'paolo10.ollari@itis.pr.it', '3386838900', 1, 'ollaripaolo63.pdf', 1, 4),
(12, 'Ollari', 'Paolo11', '0000-00-00 00:00:00', '1234567', 2010, 'Parma', 'paolo11.ollari@itis.pr.it', '3386838900', 1, 'ollaripaolo63.pdf', 1, 4),
(13, 'Ollari', 'Paolo12', '2019-02-26 00:00:00', '1234567', 2010, 'Parma', 'paolo12.ollari@itis.pr.it', '3386838900', 0, 'ollaripaolo63.pdf', 1, 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `tindirizzistudio`
--

CREATE TABLE `tindirizzistudio` (
  `ID` int(11) NOT NULL,
  `descrizione` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tindirizzistudio`
--

INSERT INTO `tindirizzistudio` (`ID`, `descrizione`) VALUES
(1, 'Informatica e telecomunicazioni'),
(2, 'Logistica e trasporti'),
(3, 'Chimica materiali e biotecnologie'),
(4, 'Elettronica e elettrotecnica'),
(5, 'Meccanica meccantronica e energia');

-- --------------------------------------------------------

--
-- Struttura della tabella `toccupazioni`
--

CREATE TABLE `toccupazioni` (
  `ID` int(11) NOT NULL,
  `descrizione` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `toccupazioni`
--

INSERT INTO `toccupazioni` (`ID`, `descrizione`) VALUES
(1, 'non occupato'),
(2, 'occupato'),
(3, 'università'),
(4, 'università disponibile');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `tdiplomati`
--
ALTER TABLE `tdiplomati`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `tdiplomati`
--
ALTER TABLE `tdiplomati`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
