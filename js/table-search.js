
function search() {
    const option = document.querySelector('#field-name').value;
    const fields = document.querySelectorAll(`td[data-option="${option}"]`);
    const keyword = document.querySelector('#search-field').value.toLowerCase();
    const strict = document.querySelector('#is-strict').checked;
    let expression = keyword;
    
    if (strict) {
        expression = `^${expression}`;
    }

    const regex = new RegExp(expression);

    requestAnimationFrame(() => {
        fields.forEach((el) => {
            const found = regex.test(el.textContent.toLowerCase());
            el.parentElement.style.display = (found) ? 'table-row': 'none';
        });
    })
}

function initListeners() {
    const searchField = document.querySelector('#search-field');
    const strictCheck = document.querySelector('#is-strict');
    const optionField = document.querySelector('#field-name');

    searchField.addEventListener('keyup', search);
    strictCheck.addEventListener('click', search);
    optionField.addEventListener('change', search);
}

window.addEventListener('load', () => {
    initListeners();
});
