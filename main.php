<?php
  require("setup.php");
  $smarty = new SmartyItis;
  $smarty->ValidateSession();



  switch($_SESSION["auth"])
  {
    case AZIENDA:
      $smarty->display("aziende.tpl");
      break;
    case DIPLOMATO:
      $smarty->display("diplomati.tpl");
      break;
    case VISITATORE:
      $smarty->display("visitatore.tpl");
      break;
  }

?>
