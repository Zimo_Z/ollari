<?php
require('libs/Smarty.class.php');

define("MYHOME", $_SERVER["DOCUMENT_ROOT"]. "/");
error_reporting(E_ERROR | E_WARNING | E_PARSE);

define ("AZIENDA","azienda");
define ("DIPLOMATO","diplomato");
define ("VISITATORE","visitatore");

/*
 * Bolo - SmartyItis
 * 
 * Modificata la classe padre: Smarty -> SmartyBC
 */
class SmartyItis extends SmartyBC
{
  var $comment_char = "#";
  // accesso alle variabili di configurazione 
  var $conf;
  //Accesso al database
  var $pdo;

  function ReadConfig()
  {

    $fp = fopen( MYHOME . "configs/config.conf", "r");
    if( $fp )
    {
      while(!feof($fp))
      {
        $line = trim(fgets($fp));
        if ($line && !preg_match("/^$this->comment_char/", $line)) 
        {
          $pieces = explode("=", $line);
          $option = trim($pieces[0]);
          $value = trim($pieces[1]);
          #echo "option $option = $value <BR>";
          $this->conf[$option] = $value; 
        }
      }
      fclose($fp);
    }
  }


  function gopage($page)
  {
    header( "Location: " . $this->conf['sito'] ."/" . $page);

  }

  public function SmartyItis()
  {
    parent::__construct();
    $this->template_dir = MYHOME . 'templates/';
    $this->compile_dir = MYHOME . 'templates_c/';
    $this->config_dir = MYHOME . 'configs/';
    $this->cache_dir = MYHOME . 'cache/';
    $this->caching = false;
    $this->assign('DiplomatiItis', 'Diplomati ITIS');
    $this->ReadConfig();
    session_start();
  }

  function connectdb()
  {
    // instantiate the pdo object
    try {
      if($this->conf["dbtype"] == "mysql")
      {
        $dsn = "{$this->conf['dbtype']}:host={$this->conf['dbserver']};dbname={$this->conf['dbname']};charset=UTF8";
      }
      else
      {
        $dsn = "{$this->conf['dbtype']}:{$this->conf['dbname']}";
      }
      $this->pdo =  new PDO($dsn,$this->conf["dbuser"],$this->conf["dbpass"]);

    } catch (PDOException $e) {
      print "Error!: " . $e->getMessage();
      die();
    } 

  }

  function ValidateSession()
  {
    //Se la sessione non e' valida ridirigo su login
    if( !isset($_SESSION['user']))
    {
      // header( "Location: " . $this->conf['sito'] . "/index.php");
      header( "Location: " . $this->conf['sito'] . "/login.php?user=".$_REQUEST['user']);
    }
  }


  /*
     Tenta l'autenticazione in questo ordine:
     - oreuser
   */
  function Autentication($usr,$pwd)
  {
    $row = NULL;
    $count =0 ;
    //Eseguo autenticazione attraverso il mio DB
    $query ="SELECT * FROM  Taziende 
     WHERE  email = '$usr' and password = '$pwd'";
    $ret=$this->pdo->query($query);
    $count = $ret->rowCount();
    if($count)
    {
      $row = $ret->fetch();
      $_SESSION['auth']=AZIENDA;
      return $row;
    }
    
    $query ="SELECT * FROM  Tdiplomati
     WHERE  email = '$usr' and password = '$pwd'";
    $ret=$this->pdo->query($query);
    $count = $ret->rowCount();
    if($count)
    {
      $_SESSION['auth']=DIPLOMATO; 
      $row = $ret->fetch();
      return $row;
    }
    
    return null;

  }


  function validate()
  {
    $utente = $_POST["user"];
    $password = $_POST["passwd"];

    if(!strlen($utente) || !strlen($password))
    {
      return false;
    }

    $row = $this->Autentication($utente,$password);
    try
    {
      
      if($row)
      {
        //Mi memorizzo le informazioni di sessione
        $_SESSION['user']= $row['email'];
        $_SESSION['userid']= $row['ID'];
        $_SESSION['Company']= $row['denominazione'];
        $_SESSION['login']= 1;
        return true;

      }

    } catch (PDOException $e)
    {
      print "Error!: " . $e->getMessage();
      echo "Err ";
      return false;
    }

    return false;
  }

  //Passare Sql che ritorna nella prima collonna il numero di record totali
  function CalcoloPagine($sql)
  {
    $totpage=0;
    $ret = $this->pdo->query($sql);
    if($ret)
    {
      
      $nrecord = $ret->fetchColumn(0);
      $totpage = (int)(($nrecord) / $this->conf["recordforpage"]) ;
      $pages = Array();
      for($i=0; $i< $totpage; $i++)
      {
        $pages[$i*$this->conf["recordforpage"]]=$i+1;
      }
      
      $this->assign("pages",$pages);
      $this->assign("totpage",$totpage);
    }


  }

  function GetNameCurriclum($id , &$name )
  {
    $store ="";
    //è presente il file del curriculum creo il nome con ID 
    $name = str_pad($id,8,'0',STR_PAD_LEFT). ".pdf";
    //Lo salvo nella directory di salvataggio 
    $store = $this->conf["datastore"] . "/" . $name;
    return $store;
  }

  function GetListBox()
  {
    //Elenco indirizzo 
    $sql ="SELECT ID,descrizione 
      FROM Tindirizzistudio
      ORDER BY descrizione";

    $ret = $this->pdo->query($sql);
    if($ret)
    {
      $d = $ret->fetchAll(PDO::FETCH_KEY_PAIR);
      $this->assign("indirizzistudio",$d);
    }

    //Elenco Elenco occupazioni 
    $sql ="SELECT ID,descrizione 
      FROM Toccupazioni
      ORDER BY descrizione";

    $ret = $this->pdo->query($sql);
    if($ret)
    {
      $d = $ret->fetchAll(PDO::FETCH_KEY_PAIR);
      $this->assign("occupazioni",$d);
    }

  }

}


?>
