<?php
/* Smarty version 3.1.33, created on 2019-06-05 18:55:16
  from 'C:\xampp\htdocs\templates\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cf7f3f49f4ef2_75162259',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9094ad020893868b2f56300240199ac3db83674e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\templates\\header.tpl',
      1 => 1559753706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cf7f3f49f4ef2_75162259 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Compiled and minified CSS -->

<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1" media="(device-height: 568px)">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!--<link rel="stylesheet" type="text/css" href="/css/stile.css">-->

<link rel="stylesheet" type="text/css" media="all" href="css/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="css/trunk.css" />
<link href="css/stile.css" rel="stylesheet" type="text/css">

<?php echo '<script'; ?>
 type="text/javascript">
	if (typeof jQuery == 'undefined')
		document.write(unescape("%3Cscript src='js/jquery-1.9.js'" + 
															"type='text/javascript'%3E%3C/script%3E"))
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="js/trunk.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/waypoints.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/jquery.counterup.min.js"><?php echo '</script'; ?>
>
<!--[if lt IE 9]>
<?php echo '<script'; ?>
 src="js/html5shiv.js"><?php echo '</script'; ?>
>
<![endif]-->

 <?php echo '<script'; ?>
>
        jQuery(document).ready(function($) {
            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
        });
    <?php echo '</script'; ?>
>
<?php }
}
