<?php
/* Smarty version 3.1.33, created on 2019-04-15 19:21:10
  from 'C:\xampp\htdocs\templates\profilo.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb4bd863b5fd3_68447577',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '002aed2e3b6ac1c01d9df2b9b39f83d5f375a4d0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\templates\\profilo.tpl',
      1 => 1555348867,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:intestazione.tpl' => 1,
    'file:menu.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5cb4bd863b5fd3_68447577 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),1=>array('file'=>'C:\\xampp\\htdocs\\libs\\plugins\\function.html_options.php','function'=>'smarty_function_html_options',),));
?>

<?php
$_smarty_tpl->smarty->ext->configLoad->_loadConfigFile($_smarty_tpl, "config.conf", null, 0);
?>

<html>
<head >
<title> Diplomati ITIS </title>


<?php $_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
<div class="container">


<!--<header id="header2" class="slide">     
		<ul id="navToggle" class="burger slide">  
    <li></li><li></li><li></li>
		</ul>-->

<?php $_smarty_tpl->_subTemplateRender("file:intestazione.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
if ($_SESSION['auth'] == @constant('AZIENDA')) {?>
  <h1>AZIENDE ITIS - Profilo</h1>
<?php } elseif ($_SESSION['auth'] == @constant('DIPLOMATO')) {?>
  <h1>DIPLOMATI ITIS - Profilo</h1>
<?php }?> 
<!--<h1><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'titolo');?>
 - Profilo</h1>-->

<?php echo '<script'; ?>
 language="JavaScript">

  function validate(f)
  {
    
    if(f.password.value != f.passwordconfirm.value)
    {
      alert("La password non coicide");
      return false;
    }

    return true;
  }

<?php echo '</script'; ?>
>

</header>

<?php $_smarty_tpl->_subTemplateRender("file:menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="content slide">     <!--	Add "slideRight" class to items that move right when viewing Nav Drawer  -->
  
		<ul class="responsive">

			<li class="header-section">
			</li>

<li class="body-section">
<?php if ($_smarty_tpl->tpl_vars['error']->value) {?><p id=error> ERROR: <?php echo $_smarty_tpl->tpl_vars['error']->value;?>
 <?php }?>

<form name=profile action="<?php echo $_smarty_tpl->tpl_vars['SCRIPT_NAME']->value;?>
?action=update" method=post onsubmit="return validate(this);" enctype="multipart/form-data">
<button id="button1" type="submit" class="btn btn-primary btn-lg">Salva</button>
<button  onclick="window.location='index.php'" id="button2" type="reset" class="btn btn-danger btn-lg">Annulla</button>
<input type=hidden name=id value=<?php echo $_smarty_tpl->tpl_vars['valori']->value['ID'];?>
>



<table class="table-fill">

<?php if ($_SESSION['auth'] == @constant('AZIENDA')) {?>

  <tr> 
    <td>Denominazione</td>
    <td><input name=denominazione type=text value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['denominazione'];?>
" required="required" ></td>
  </tr>
  <tr> 
    <td>Residenza</td>
    <td><input name=residenza type=text value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['residenza'];?>
" required="required"></td>
  </tr>
  <tr> 
    <td>Sito</td>
    <td><input name=sito type=text value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['sito'];?>
"></td>
  </tr>

<?php } elseif ($_SESSION['auth'] == @constant('DIPLOMATO')) {?>
  <tr> 
    <td>Cognome</td>
    <td><input name=cognome type=text value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['cognome'];?>
" >
    </td>
  </tr>
  <tr> 
    <td>Nome</td>
    <td><input name=nome type=text value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['nome'];?>
" ></td>
  </tr>
  <tr> 
    <td>Data di nascita</td>
    <td><input name=datanascita type=date  value='<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['valori']->value['datanascita'],"%Y-%m-%d");?>
' ></td>
  </tr>
  <tr> 
    <td>Anno di diploma</td>
    <td><input name=annodiploma type=number  value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['annodiploma'];?>
" ></td>
  </tr>
  <tr> 
    <td>Cellulare</td>
    <td><input name=cellulare type=number  value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['cellulare'];?>
" ></td>
  </tr>
  <tr> 
    <td>Laurea</td>
    <td><select  name=laurea value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['laurea'];?>
">
    <option value="" selected disabled hidden>Seleziona</option>
  <option value="1">Si</option>
  <option value="2">No</option>
</select></td>
  </tr>
  <tr> 
    <td>Curriculum</td>
    <td>
    <?php if ($_smarty_tpl->tpl_vars['valori']->value['curriculumlink']) {?>
      <a href=profilo.php?download=curriculum><img src="img/pdf.png">File attuale</a>
    <?php } else { ?>
    <img src="img/nopdf.png">
    Nessun curriculum allegato
    <?php }?> 
    <p STYLE="text-align:right"><input name=curriculumfile type=file ></td></p>
  </tr>

  <tr> 
    <td>Occupazione</td>
    <td>
      <select name=ID_Toccupazioni>
      <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['occupazioni']->value,'selected'=>$_smarty_tpl->tpl_vars['valori']->value['ID_Toccupazioni']),$_smarty_tpl);?>

      </select>
      </td>
  </tr>


<?php }?>
  <tr> 
    <td>Descrizione</td>
    <td>
      <select name=ID_Tindirizzistudio>
      <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['indirizzistudio']->value,'selected'=>$_smarty_tpl->tpl_vars['valori']->value['ID_Tindirizzistudio']),$_smarty_tpl);?>

      </select>
      </td>
  </tr>

  <tr> 
    <td>E-mail</td>
    <td><input name=email type=text value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['email'];?>
" disabled ></td>
  </tr>

  <tr> 
    <td>Password</td>
    <td><input name=password type=password value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['password'];?>
" 
      pattern=".{6,}"  title="Almeno 6 caratteri">
      </td>
  </tr>
  
  <tr> 
    <td>Conferma password</td>
    <td><input name=passwordconfirm type=password value="<?php echo $_smarty_tpl->tpl_vars['valori']->value['password'];?>
" 
      pattern=".{6,}"  title="Almeno 6 caratteri">
   </td>
  </tr>
  

</table>

</form>


</li>
			<li class="footer-section">
				<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
			</li>
		</ul>
	</div>
</div>

</body>
</html>

<?php }
}
